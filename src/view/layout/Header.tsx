import { useState } from "react";
import Search from "../../assets/icon/Search";

const Header = () => {
  const [search, setSearch] = useState(false);
  return (
    <header id="header">
      <nav className="nav-header">
        <div className="nav-left">
          <h1>Hello John Dae</h1>
          <p>View and control your finance here!</p>
        </div>
        <div className="nav-right">
          <div className="search">
            <button className="btn-search" onClick={() => setSearch(!search)}>
              <Search />
            </button>
            <div className={`box-input ${search ? "btn-seacrh-active" : ""}`}>
              <input type="text" className="" placeholder="Type here....." />
            </div>
          </div>
        </div>
      </nav>
    </header>
  );
};

export default Header;
