import { NavLink } from "react-router-dom";
import Home from "../../assets/icon/Home";
import Notif from "../../assets/icon/Notif";
import Time from "../../assets/icon/Time";
import User from "../../assets/icon/User";
import Payment from "../../assets/icon/Payment";
import Setting from "../../assets/icon/Setting";
import Message from "../../assets/icon/Message";
import Logout from "../../assets/icon/Logout";

const Aside = () => {
  return (
    <aside className="nav-let">
      <div className="nav-let-top">
        <h1 className="brand">S.</h1>
        <NavLink to="/message">
          <Message />
        </NavLink>
      </div>
      <div className="nav-let-center">
        <ul className="navigation">
          <li>
            <NavLink to="/">
              <Home />
            </NavLink>
          </li>
          <li>
            <NavLink to="/notif">
              <Notif />
            </NavLink>
          </li>
          <li>
            <NavLink to="/time">
              <Time />
            </NavLink>
          </li>
          <li>
            <NavLink to="/member">
              <User />
            </NavLink>
          </li>
          <li>
            <NavLink to="/payment">
              <Payment />
            </NavLink>
          </li>
          <li>
            <NavLink to="/setting">
              <Setting />
            </NavLink>
          </li>
        </ul>
      </div>
      <div className="nav-let-bottom">
        <ul className="nav-profil">
          <li>
            <NavLink to="">
              <img src="https://i.ibb.co/QKNNjCJ/7309689.jpg" alt="" />
            </NavLink>
          </li>
          <li>
            <NavLink to="/signout">
              <Logout />
            </NavLink>
          </li>
        </ul>
      </div>
    </aside>
  );
};

export default Aside;
