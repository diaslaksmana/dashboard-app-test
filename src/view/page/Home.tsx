import { Bar, Doughnut } from "react-chartjs-2";
import Clip from "../../assets/icon/Clip";
import Emoji from "../../assets/icon/Emoji";
import List from "../../assets/icon/List";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ArcElement,
} from "chart.js";
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
  ArcElement
);
const Home = () => {
  const labels = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
  ];
  const data = {
    labels: labels,
    datasets: [
      {
        label: "Expenses by Month",
        data: [65, 59, 80, 81, 56, 55, 40],
        backgroundColor: ["rgb(153, 102, 255)"],
        borderColor: ["rgb(153, 102, 255)"],
        borderWidth: 1,
      },
      {
        label: "Expenses by Month",
        data: [65, 59, 80, 81, 56, 55, 40],
        backgroundColor: ["rgb(174, 162, 199)"],
        borderColor: ["rgb(174, 162, 199)"],
        borderWidth: 1,
      },
    ],
  };
  const data2 = {
    labels: ["Done", "In Progress", "ToDo"],
    datasets: [
      {
        label: "# of Votes",
        data: [12, 19, 3],
        backgroundColor: [
          "rgba(255, 99, 132, 0.2)",
          "rgba(54, 162, 235, 0.2)",
          "rgba(255, 206, 86, 0.2)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
        ],
        borderWidth: 1,
      },
    ],
  };

  return (
    <div className="home">
      {/* Row 1 */}
      <div className="row row-one">
        <div className="col">
          <div className="card">
            <h1 className="title-card">Balance statistic</h1>
            <div className="card-row">
              <div className="card-left">
                <h1>$543</h1>
                <p className="title">
                  <img
                    src="https://i.ibb.co/gDnQy4d/2812111-bank-cash-coin-dollar-money-icon.png"
                    alt=""
                  />
                  your total balance
                </p>
                <label htmlFor="">Always see your earning update</label>
              </div>
              <div className="card-right">
                <Bar data={data} options={{ responsive: true }} />
              </div>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card-custom">
            <div className="title">
              <h1>S.</h1>
              <h2>Visa</h2>
            </div>
            <div className="overview">
              <div className="name">
                <p>**********676876</p>
                <h1>John D</h1>
              </div>
              <p>09/24</p>
            </div>
          </div>
        </div>
      </div>

      {/* Row 2 */}
      <div className="row row-two">
        <div className="col card-left">
          <h1 className="title-card">List of items to buy</h1>
          <div className="date">
            <button>
              <h1>02.00</h1>
              <p>Sat, Aug 2023</p>
            </button>
            <label htmlFor=""></label>
            <button>
              <h1>05.00</h1>
              <p>Sat, Aug 2023</p>
            </button>
          </div>
          <div className="add-shop">
            <p>
              <span>0/3 </span>Shopping
            </p>
            <button>+ Add an item</button>
          </div>
          <div className="list-item">
            <ul>
              <li>
                <div className="checkbox">
                  <div className="form-check">
                    <input type="checkbox" />
                    <label htmlFor="">Mackbook</label>
                  </div>
                  <button>
                    <List />
                  </button>
                </div>
              </li>
              <li>
                <div className="checkbox">
                  <div className="form-check">
                    <input type="checkbox" />
                    <label htmlFor="">Bicycle</label>
                  </div>
                  <button>
                    <List />
                  </button>
                </div>
              </li>
              <li>
                <div className="checkbox">
                  <div className="form-check">
                    <input type="checkbox" />
                    <label htmlFor="">Motorcycle</label>
                  </div>
                  <button>
                    <List />
                  </button>
                </div>
              </li>
              <li>
                <div className="checkbox">
                  <div className="form-check">
                    <input type="checkbox" />
                    <label htmlFor="">Iphone 14 pro max</label>
                  </div>
                  <button>
                    <List />
                  </button>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="col card-right">
          <h1 className="title-card">Easter Howard</h1>
          <div className="mini-message">
            <div className="me">
              <p>Are you ready</p>
              <img src="https://i.ibb.co/QKNNjCJ/7309689.jpg" alt="" />
            </div>
            <div className="other-people">
              <img src="https://i.ibb.co/QKNNjCJ/7309689.jpg" alt="" />
              <p>I have prepare everything</p>
            </div>
          </div>
          <div className="input-text">
            <input
              type="text"
              name=""
              id=""
              placeholder="type your message...."
            />
            <div className="message-tools">
              <div className="tools-icon">
                <button>
                  <Emoji />
                </button>
                <button>
                  <Clip />
                </button>
              </div>
              <button className="btn-send-message">Send message</button>
            </div>
          </div>
        </div>
      </div>

      {/* Row 3 */}
      <div className="row row-three">
        <div className="col col-left">
          <div className="card">
            <div className="title">
              <div className="title-card">
                <h1 className="title-card">List of items to buy</h1>
              </div>
              <div className="title-sort">
                <button>Newest</button>
                <button>Oldest</button>
              </div>
            </div>
            <ul className="list-transaction">
              {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((item) => (
                <li key={item}>
                  <div className="list-user">
                    <div className="cover">
                      <img src="https://i.ibb.co/QKNNjCJ/7309689.jpg" alt="" />
                    </div>
                    <div className="overview">
                      <h1>John Dae</h1>
                      <p>03 july 2023</p>
                    </div>
                  </div>
                  <div className="list-total">
                    <h1>-$3.000</h1>
                    <List />
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className="col col-right">
          <div className="card">
            <div className="title">
              <div className="title-card">
                <h1 className="title-card">Analytics</h1>
              </div>
              <List />
            </div>
            <Doughnut data={data2} options={{ responsive: true }} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
