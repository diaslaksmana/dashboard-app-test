import { useState } from "react";
import "./App.css";
import Router from "./router/Index";
import Aside from "./view/layout/Aside";
import Headder from "./view/layout/Header";
import { NavLink } from "react-router-dom";

function App() {
  const [togle, setTogle] = useState(false);
  return (
    <div className="layout">
      <div className="aside">
        <Aside />
      </div>
      <main className="page">
        <div className="page-center ">
          <div className="container">
            <Headder />
            <Router />
          </div>
        </div>
        <div className={`page-right ${togle ? "active-togle" : ""}`}>
          <div className="content ">
            <div className="container">
              <div className="card card-border expense-income">
                <h1 className="title-card">Expense and Income</h1>
                <ul>
                  <li>
                    <div className="expense">
                      <p>Expense</p>
                      <h1>75%</h1>
                      <label>5.564</label>
                    </div>
                  </li>
                  <li className="separate">VS</li>
                  <li>
                    <div className="income">
                      <p>Income</p>
                      <h1>40%</h1>
                      <label>5.564</label>
                    </div>
                  </li>
                </ul>
                <div className="progress-status">
                  <div className="expense"></div>
                  <div className="income"></div>
                </div>
              </div>

              <div className="card card-border last-spending">
                <h1 className="title-card">Last Spending</h1>
                <ul>
                  <li>
                    <div className="list-spending">
                      <div className="cover">
                        <img
                          src="https://i.ibb.co/TBGCdxm/2303176-ecommerce-eshop-house-retail-shop-icon.png"
                          alt=""
                        />
                      </div>
                      <div className="overview">
                        <NavLink to="/detail">
                          <h1>Online Store</h1>
                          <p>May 30, 2023 at 08.00 PM</p>
                        </NavLink>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div className="list-spending">
                      <div className="cover">
                        <img
                          src="https://i.ibb.co/HF5g3BD/8726120-hospital-icon.png"
                          alt=""
                        />
                      </div>
                      <div className="overview">
                        <NavLink to="/detail">
                          <h1>Pay the hospital</h1>
                          <p>May 30, 2023 at 08.00 PM</p>
                        </NavLink>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div className="list-spending">
                      <div className="cover">
                        <img
                          src="https://i.ibb.co/9G5zdD8/8726422-ticket-icon.png"
                          alt=""
                        />
                      </div>
                      <div className="overview">
                        <NavLink to="/detail">
                          <h1>Ticket</h1>
                          <p>May 30, 2023 at 08.00 PM</p>
                        </NavLink>
                      </div>
                    </div>
                  </li>
                </ul>
                <div className="text-center">
                  <NavLink to="" className="btn-view-all-spending">
                    View all
                  </NavLink>
                </div>
              </div>

              <div className="card card-border premium-member">
                <h1 className="text-center">Goto Premium</h1>
                <img
                  src="https://i.ibb.co/dJjCZcJ/11057096-monarchy-queen-royal-crown-royalty-icon.png"
                  alt=""
                />
                <h2>need more feature?</h2>
                <p>Update your account to premium ti get your feature</p>
                <NavLink to="">Get now</NavLink>
              </div>
            </div>
          </div>
        </div>
        <button onClick={() => setTogle(!togle)} className="togle-right-side">
          ?
        </button>
      </main>
    </div>
  );
}

export default App;
