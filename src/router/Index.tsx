import { Route, Routes } from "react-router-dom";
import Home from "../view/page/Home";
import Setting from "../view/page/Setting";
import Notification from "../view/page/Notification";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/notif" element={<Notification />} />
      <Route path="/setting" element={<Setting />} />
    </Routes>
  );
};
export default Router;
